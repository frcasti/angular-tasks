import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Componentes
import { ListsComponent } from './components/list/lists.component';

const appRoutes: Routes = [
	{path: '', component: ListsComponent},
	// {path: 'home', component: HomeComponent},
	// {path: 'productos', component: ProductosListComponent},
	// {path: 'crear-producto', component: ProductoAddComponent},
	// {path: 'producto/:id', component: ProductoDetailComponent},
	// {path: 'editar-producto/:id', component: ProductoEditComponent},
	// {path: '**', component: ErrorComponent}
];

export const appRoutingProviders: any[] = [];
export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);