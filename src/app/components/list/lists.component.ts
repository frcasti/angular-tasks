import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ListService } from '../../services/list.service';
import { List } from '../../models/list';

@Component({
  selector: 'app-lists',
  templateUrl: '../../views/lists.component.html',
  styleUrls: ['./lists.component.css']
})

export class ListsComponent implements OnInit {
	public lists: List[];

  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _listService: ListService
  ) {}

  ngOnInit() {
  	this.getLists();
  }

  getLists() {
    console.log('Entrando en getLists');
    this._listService.getLists().subscribe(
      result => {
        
        if(result.code != 200){
          console.log(result);
        }else{
          // this.lists = result.data;
          console.log(result);
        }

      },
      error => {
        console.log(<any>error);
      }
    );
  }

}
